1. 启动rpc-server下的 ServerApplication
2. 启动rpc-client下的 ClientApplication
3. 访问 [http://127.0.0.1:8080/user/{id}](http://127.0.0.1:8080/user/1)
4. id 包含｛1，2｝
5. 会看到RPC 调用的轮询结果
