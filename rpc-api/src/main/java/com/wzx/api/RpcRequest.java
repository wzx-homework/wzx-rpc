package com.wzx.api;

import lombok.SneakyThrows;

public class RpcRequest {
    private String serviceName;
    private String methodName;
    private Object[] args;
    private String[] argTypes;

    public void setParamTypes(Class[] pts) {
        argTypes = new String[pts.length];
        for (int i = 0; i < pts.length; i++) {
            argTypes[i] = pts[i].getName();
        }
    }

    @SneakyThrows
    public Class[] getParamTypes() {
        Class[] pts = new Class[argTypes.length];
        for (int i = 0; i < pts.length; i++) {
            pts[i] = Class.forName(argTypes[i]);
        }
        return pts;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

}
