package com.wzx.client;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientApplication {

    /**
     * 提供的服务端端口
     */
    public static int[] serverPorts = {999, 998};

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }




//    @Autowired
//    public void test(UserService userService) throws InterruptedException {
//        for (int i = 0; i < 100; i++) {
//            System.out.println(userService.getById(i % 2 + 1));
//            Thread.sleep(3000);
//        }
//    }
}
