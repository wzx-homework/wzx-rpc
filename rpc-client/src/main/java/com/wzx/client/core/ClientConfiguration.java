package com.wzx.client.core;

import com.wzx.api.RpcRequest;
import com.wzx.api.Utils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Proxy;

@Configuration
public class ClientConfiguration {

    @Bean
    public UserService userService(RpcClient rpcClient) {
        return (UserService) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{UserService.class}, (proxy, method, args) -> {
            RpcRequest rpcRequest = new RpcRequest();
            rpcRequest.setServiceName("userService");

            rpcRequest.setMethodName(method.getName());
            rpcRequest.setArgs(args);
            rpcRequest.setParamTypes(method.getParameterTypes());

            String ret = rpcClient.request(rpcRequest);
            Class<?> returnType = method.getReturnType();

            return Utils.objectMapper.readValue(ret, returnType);
        });
    }
}
