package com.wzx.client.core;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class RpcClientHandler extends SimpleChannelInboundHandler<String> {

    ChannelHandlerContext ctx;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        System.out.println("客户端：收到消息了");
        RpcNioSocketChannel channel = (RpcNioSocketChannel) ctx.channel();
        synchronized (channel) {
            channel.setResult(msg);
            channel.notify();
        }
    }
}
