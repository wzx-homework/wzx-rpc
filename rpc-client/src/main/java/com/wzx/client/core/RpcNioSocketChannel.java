package com.wzx.client.core;

import io.netty.channel.socket.nio.NioSocketChannel;

public class RpcNioSocketChannel extends NioSocketChannel {

    private String result;
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
