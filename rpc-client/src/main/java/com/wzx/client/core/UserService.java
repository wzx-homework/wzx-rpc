package com.wzx.client.core;

import com.wzx.api.RpcService;
import com.wzx.api.User;

@RpcService
public interface UserService {
    Object getById(Integer id);
}
