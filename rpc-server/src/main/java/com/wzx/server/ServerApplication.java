package com.wzx.server;

import com.wzx.server.core.RpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

//    @Autowired
//    public void start(RpcServer rpcServer) throws InterruptedException {
//        rpcServer.startServer();
//    }

    @Bean
    public RpcServer rpcServer1() throws InterruptedException {
        RpcServer rpcServer = new RpcServer(999);
        rpcServer.startServer();
        System.out.println("999端口启动");
        return rpcServer;
    }

    @Bean
    public RpcServer rpcServer2() throws InterruptedException {
        RpcServer rpcServer = new RpcServer(998);
        rpcServer.startServer();
        System.out.println("998端口启动");
        return rpcServer;
    }

}
