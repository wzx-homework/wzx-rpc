package com.wzx.server.core;

import com.wzx.api.RpcService;
import com.wzx.api.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@RpcService
@Component
public class UserService {

    static Map<Integer, User> userMap = new HashMap<>();
    static {
        userMap.put(1, new User(1, "张学友"));
        userMap.put(2, new User(2, "周杰伦"));
    }

    public User getById(Integer id) {
        return userMap.get(id);
    }
}
